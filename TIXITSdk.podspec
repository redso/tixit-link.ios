#
# Be sure to run `pod lib lint TIXITSdk.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'TIXITSdk'
  s.version          = '1.0.2'
  s.summary          = 'Redso Private Pod for Link Reserve'
  s.description      = <<-DESC
Redso Private Pod for Link Reserve
                       DESC

  s.homepage         = 'https://bitbucket.org/redso/tixit-link.ios'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'git' => 'royng@redso.com.hk' }
  s.source           = { :git => 'https://bitbucket.org/redso/tixit-link.ios.git', :tag => s.version.to_s }

  s.ios.deployment_target = '8.0'

  s.source_files = 'TIXITSdk/Classes/**/*'
end
