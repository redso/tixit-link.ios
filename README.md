# TIXITSdk

[![CI Status](http://img.shields.io/travis/git/TIXITSdk.svg?style=flat)](https://travis-ci.org/git/TIXITSdk)
[![Version](https://img.shields.io/cocoapods/v/TIXITSdk.svg?style=flat)](http://cocoapods.org/pods/TIXITSdk)
[![License](https://img.shields.io/cocoapods/l/TIXITSdk.svg?style=flat)](http://cocoapods.org/pods/TIXITSdk)
[![Platform](https://img.shields.io/cocoapods/p/TIXITSdk.svg?style=flat)](http://cocoapods.org/pods/TIXITSdk)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

TIXITSdk is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'TIXITSdk'
```

## Author

git, royng@redso.com.hk

## License

TIXITSdk is available under the MIT license. See the LICENSE file for more info.
