import UIKit
import WebKit

public protocol BookingViewControllerDelegate: class {
  func bookingViewControllerDidDismiss(_ bookingViewController: BookingViewController)
  func bookingViewControllerDidFail(_ bookingViewController: BookingViewController)
}

public class BookingViewController: UIViewController {
  public enum BookingViewControllerLanguage: String {
    case en = "en", zhHant = "zh-Hant", zhHans = "zh-Hans"
  }
  public weak var delegate: BookingViewControllerDelegate?
  public var eventId: String?
  public var language: BookingViewControllerLanguage = BookingViewControllerLanguage.en
  
  let contentView = WKWebView()
  
  override public func viewDidLoad() {
    super.viewDidLoad()
    view.backgroundColor = UIColor.white
    contentView.translatesAutoresizingMaskIntoConstraints = false
    view.addSubview(contentView)
    NSLayoutConstraint(item: contentView, attribute: .top, relatedBy: .equal, toItem: topLayoutGuide, attribute: .bottom, multiplier: 1, constant: 0).isActive = true
    NSLayoutConstraint(item: contentView, attribute: .bottom, relatedBy: .equal, toItem: bottomLayoutGuide, attribute: .top, multiplier: 1, constant: 0).isActive = true
    NSLayoutConstraint(item: contentView, attribute: .leading, relatedBy: .equal, toItem: view, attribute: .leading, multiplier: 1, constant: 0).isActive = true
    NSLayoutConstraint(item: contentView, attribute: .trailing, relatedBy: .equal, toItem: view, attribute: .trailing, multiplier: 1, constant: 0).isActive = true
    contentView.navigationDelegate = self
  }
  
  public override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    var link = "https://link-reserve-dev.redso.com.hk"
    if let eventId = eventId { link = link + "/events/\(eventId)" }
    let now = Int64(Date().timeIntervalSince1970 * 1000)
    let code = MD5("\(now)|||r2edsoli0nkTIX1IT-rese7rve")
    var urlComps = URLComponents(string: link)!
    var queryItems = [URLQueryItem]()
    queryItems.append(URLQueryItem(name: "t", value: "\(now)"))
    queryItems.append(URLQueryItem(name: "code", value: code))
    queryItems.append(URLQueryItem(name: "lang", value: language.rawValue))
    urlComps.queryItems = queryItems
    
    let url = urlComps.url!
    let request = URLRequest(url: url)
    contentView.load(request)
  }
  
}

extension BookingViewController: WKNavigationDelegate {
  
  public func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
    if let url = navigationAction.request.url?.absoluteString {
      if url.contains("returnto.redsoapp.com") {
        dismiss(animated: true) {
          self.delegate?.bookingViewControllerDidDismiss(self)
        }
        decisionHandler(.cancel)
        return
      }
    }
    decisionHandler(.allow)
  }
  
  public func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
    dismiss(animated: true) {
      self.delegate?.bookingViewControllerDidFail(self)
    }
  }
  
}
