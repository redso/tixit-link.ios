import UIKit
import TIXITSdk

class ViewController: UIViewController, BookingViewControllerDelegate {
  
  override func viewDidLoad() {
    super.viewDidLoad()
  }
  
  @IBAction func demo() {
    let bookingViewController = BookingViewController()
    bookingViewController.delegate = self
    bookingViewController.language = .en
    present(bookingViewController, animated: true, completion: nil)
  }
  
  @IBAction func demoZhHant() {
    let bookingViewController = BookingViewController()
    bookingViewController.delegate = self
    bookingViewController.language = .zhHant
    present(bookingViewController, animated: true, completion: nil)
  }
  
  @IBAction func demoZhHans() {
    let bookingViewController = BookingViewController()
    bookingViewController.delegate = self
    bookingViewController.language = .zhHans
    present(bookingViewController, animated: true, completion: nil)
  }
  
  @IBAction func demoWithTargetEvent() {
    let bookingViewController = BookingViewController()
    bookingViewController.delegate = self
    bookingViewController.language = .zhHant
    bookingViewController.eventId = "5679759568666624"
    present(bookingViewController, animated: true, completion: nil)
  }
  
  func bookingViewControllerDidDismiss(_ bookingViewController: BookingViewController) {
  }
  
  func bookingViewControllerDidFail(_ bookingViewController: BookingViewController) {
  }
  
}

